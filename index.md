> Now building [PlantingSpace](https://planting.space) and participating in [Polkadot Network](https://polkadot.network/).

I spend the majority of my time on PlantingSpace - a project with big ambitions - creating a framework for composition of all data and algorithms known to us, which could help us to leverage them better. This can hopefully bring us a step closer towards AI and the understanding of our own cognition.

I am also helping out with [Open Reviews Association](https://open-reviews.net), where I developed the initial standard and infrastructure to enable more open sharing of insights.

Recently I have invested in a few great projects like [ElevenLabs](https://elevenlabs.io/) and [Enso](https://enso.org/) along the theme of AI and knowledge representation, let me know if you are working on something interesting in that area that needs support!

Previously I co-founded the [Polkadot Network](https://polkadot.network/) and built/ran the organisation which supports it - [Web3 Foundation](https://web3.foundation).
The Foundation does fundamental research (cryptography, algorithms, networking, economics), works with development teams, provides grants, educates, and develops the community (e.g. via events like [Web3 Summit](https://web3summit.com/)) around decentralised web protocols.

Before starting the Web3 Foundation I worked at [Parity Technologies](https://www.parity.io/) on decentralised protocols with both public and consortium applications in mind. I primarily worked on consensus algorithms, coming up with a simple consensus algorithm [Aura](https://openethereum.github.io/wiki/Aura) and smart contract-based [validator selection mechanism](https://openethereum.github.io/Validator-Set), both of which have been used for many live applications. I also worked on privacy/confidentiality preserving protocols.

I also spent some time at [BlueCrest Capital](https://en.wikipedia.org/wiki/BlueCrest_Capital_Management) developing financial risk technology, at [Purse.io](https://purse.io/) building one of the most popular legitimate Bitcoin use-cases, and at [BAE Systems](https://www.baesystems.com/en/home) designing and creating a proof of concept tactical mesh network for the [Morpheus Programme](https://www.gov.uk/guidance/morpheus-project-next-generation-tactical-communication-information-systems-for-defence).

At the University of Oxford, I worked in the [Machine Learning Research Group](http://www.robots.ox.ac.uk/~parg/) under [Frank Wood](https://www.cs.ubc.ca/~fwood/).
I used a new approach to machine learning based on [probabilistic programming](http://www.probabilistic-programming.org) ([Anglican language](https://probprog.github.io/anglican/index.html)) to explore procedural knowledge representation and build a factoid question answering system. Before that, I worked on designing a process for [saving the essence of human brain digitally](http://www.robots.ox.ac.uk/~fwood/teaching/3YP_2014/).

Otherwise, at Oxford, I spent a bunch of time learning about and discussing philosophy with friends and at Socrates Society or [Jowett Society](https://www.philosophy.ox.ac.uk/the-philosophical-society-/-jowett-society#/).

In high school and before I spent a lot of time on physics, doing various projects as part of [Krajowy Fundusz na rzecz Dzieci](https://fundusz.org/english) such as analysing binary stars, measuring properties of conductive polymer films, building, and using laser Doppler velocimeter.

@@narrow
  ![Me](/assets/ja.jpg)
@@
