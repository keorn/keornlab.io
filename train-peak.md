# Train-peak challenge

Want a physical challenge? Do not like to follow set tracks? Like to explore new places?

## Rules

Choose a hill or mountain peak - the goal is to reach it in the shortest time using only your body for power.

You can start at any serviced train station, make sure to stand under the sign with station name before moving.

### Optional

You can restrict the mode of transport (e.g. running, cycling) to create more fine grained categories.

## Strategy

Depending on your off-road appetite choose a peak with some trail through it. You can look at peaks via apps like Windy Maps, Locus or other local mapping applications.

Look around the peak for nearby train tracks and stations. Choose a station with the most reasonable distance and ascent to the peak.

### Example



## FAQ

### Why train stations?

When reaching a peak there is not an obvious place to start from - should it be the nearby hut, designated trail head or something else? Train stations:
- are convenient to get to and have some facilities (food, toilet)
- usually designate a major valley (are at a distance from peaks)
- encourage usage of public transport
- do not fixate the start point since one can start from different stations

### Does starting from a train station does not restrict accessible areas too much?

The challenge obviously works best in areas with lots of train connections (many parts of Europe), so it may not be suitable for everyone. Otherwise train stations form convenient starting points which also ensure you do not get yourself into too extreme situations. Having a challenge with less restrictive start locations may lead people to try to conquer peaks with risky ascent by going up high with line car or driving high.
