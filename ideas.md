# Ideas I like

I, and I think most, look to make the future better based on understanding of past and present. Which part of the future and what better means, differs significantly.

I enjoy thinking about the next moment in the present to stay grounded, but also sometimes venture into the far future. Present is more personal, so here are some ideas about the future, besides the ones explored as the main goal of [PlantingSpace](https://planting.space).

## Universal means of exchange

Any individual agent is not able to supply everything that it needs, so we need a scarce, fungible resource which can be transferred digitally. It does not matter what the resource is backed by, as long as we can all agree that it should be the primary means of exchange. So far cryptocurrencies have gotten the closest to fulfilling that need. One issue is that currently we also rely on capital controls for law enforcement, given the requirement for universality and fungibility we will need new ways to enforce laws.

## Structure of collaboration

I think one of the most important problems that are not studied in a particularly systematic way is iter-humen/agent collaboration.

One interesting direction that is being explored now is [decentralised autonomous organisations](https://en.wikipedia.org/wiki/Decentralized_autonomous_organization), like [Polkadot with its governance](https://polkadot.network/features/opengov/). It continously experiments with effective coordination of a large stakeholder group, making use of clear rules and open participation.

At [PlantingSpace](https://planting.space) we are also trying to explore how to effectively collaborate, but in a smaller group and with a complex goal in mind. We hope to publish more about it as we test different paths.

## Regulation of joint resource consumption

Particular idea that I think can help us with that is **Full limited resources tax (FLRT)**.

An extension to 100% [land value tax](https://en.wikipedia.org/wiki/Land_value_tax), where more limited resources are being priced-in such as: water pollution capacity or naturally occurring minerals.

I will not delve into numerous benefits of abolishing all taxes besides 100% land value tax, but there is one drawback: it does not properly price externalities which are currently either not regulated or regulated in a complex way.

The idea behind FLRT is that the environment is something that belongs to all of us, but we still need to allocate its resources efficiently via markets. Governance of society should agree on the maximum extent to which each resource of the environment should be used e.g.: areas of land for private ownership, maximum level of CO2 released into the atmosphere, proportion of earth to be strip mined.

This would then create a market for each resource with tax on the value of each resource acquired. Statistical methods would be employed to accurately determine value of any particular lot, which should not be hard given a fairly fungible nature of resources.

This sort of setup should simplify the current regulations as well as properly price-in the current underhanded usage of natural resources.

## Individual empowerment

We could try to develop something like **Human accelerators**, by analogy to startup accelerators, which area great at encouraging new business ideas.

Any project needs energetic people to get it done. There are many important and innovative ideas being worked on, but many energetic people are too risk averse to try working on them.
People of all ages end up going to a more stable firm or stay in one for too long.

There are some nice efforts to help with this already, like [Entrepreneur First](https://www.joinef.com/), but they focus on making startups. It would be great to have a program where in the initial phase everyone works towards an understanding about what cause they would like to work for. Once the cause is identified the process of determining the best way to contribute would start. Two main paths would be possible: starting a new project or joining an existing one.
If one chooses to start a new company then it could look similar to existing accelerator programs. In many cases however, joining an existing project makes much more sense. For people joining a project there would be a program of self-driven education about the subject as well as being effective in an organization. The idea would be to create the highest possible impact when joining and become a meaningful contributor to the cause of the project.

These kinds of programs can monetize by taking company shares or eventual compensation cut.