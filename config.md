<!--
Add here global page variables to use throughout your
website.
The website_* must be defined for the RSS to work
-->
@def website_title = "Peter Czaban"
@def website_descr = "Personal website"
@def website_url   = "https://keorn.org"

@def hasplotly = false

<!--
Add here global latex commands to use throughout your
pages. It can be math commands but does not need to be.
For instance:
* \newcommand{\phrase}{This is a long phrase to copy.}
-->
\newcommand{\postheader}{
  > Updated on ~~~{{ fill rss_pubdate }}~~~
  # ~~~{{ fill rss_title }}~~~
}